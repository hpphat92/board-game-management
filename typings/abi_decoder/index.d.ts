
declare module "abi-decoder" {
    export class Event {
        name: string;
        type: string;
        value: string;
    }

    export class Log {
        name: string;
        address: string;
        events: Event[];
    }

    export function getABIs(): any[];
    export function addABI(abiArray: any): void;
    export function getMethodIDs(): {};
    export function decodeMethod(data: any): {
        name: any;
        params: {
            name: any;
            value: any;
            type: any;
        }[];
    };
    export function decodeLogs(logs: any): Log[];
    export function removeABI(abiArray: any): void;
}