import path from 'path';

export const APP_CONFIG = {
    PORT: 12345,
    DEBUG: true,
    VERSION: "0.1.0.0",
    MEMCACHE_STORE: path.join(__dirname, 'cache'),
    LOG_DIR: path.join(__dirname, 'logs')
}