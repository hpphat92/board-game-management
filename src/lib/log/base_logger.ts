import * as winston from 'winston';
import { HashTableBase } from '../../lib';
import * as path from 'path';
import DailyRotateFile from "winston-daily-rotate-file";

export enum WINSTON_LOG_LEVEL {
    error = 'error',
    warn = 'warn',
    info = 'info',
    verbose = 'verbose',
    debug = 'debug',
    silly = 'silly'
}

export class BaseLogger extends HashTableBase<winston.Logger> {
    protected _logPath: string;
    constructor(logDir: string) {
        super();
        this._logPath = logDir;
    }

    protected createLogger(filename: string, level: WINSTON_LOG_LEVEL) {
        const rotationTransport = this.getRotationFormat(filename);
        return winston.createLogger({
            level: level.toString(),
            format: winston.format.json(),
            transports: [
                rotationTransport,
                new winston.transports.Console()
            ],
            exitOnError: false
        })
    }

    private getRotationFormat(filename: string) {
        return new DailyRotateFile({
            level: 'debug',
            filename: path.join(this._logPath, filename),
            handleExceptions: true,
            datePattern: 'YYYY_MM_DD',
            json: true,
            maxSize: 5242880
        });
    }
}