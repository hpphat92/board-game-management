import express from 'express';

export function hook() {
    let route = express.Router();
    route.use(handler);
    return route;
}

function handler(req: express.Request, res: express.Response, next: express.NextFunction) {
    if (!req.headers["access-token"]) {
        res.status(401);
        res.send();
    }

    next();
}