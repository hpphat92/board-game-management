import express from "express";
import { doSend, doParseRequest } from '../lib/server';
import { AuthRequest } from "../models/auth.model";
import { container } from "tsyringe";
import { AuthController } from "../controllers/authentication";

const AUTH_CONTROLLER = container.resolve(AuthController);
const ROUTER = express.Router();

ROUTER.post("/signin", async (req, res, next) => {
    try {
        let params = doParseRequest<AuthRequest, AuthRequest>(req);
        let result = await AUTH_CONTROLLER.doAuth(params.body);
        doSend(res, result);
    } catch (err) {
        next(err);
    }
});

ROUTER.post("/register", async (req, res, next) => {
    try {
        let params = doParseRequest<AuthRequest, AuthRequest>(req);
        let result = await AUTH_CONTROLLER.doRegister(params.body);
        doSend(res, result);
    } catch (err) {
        next(err);
    }
});

export function getRouter() {
    return ROUTER;
}