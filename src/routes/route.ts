import express from "express";
import { doSend, doParseRequest } from '../lib/server';
import { TestModel } from "../models/test.model";
import { getRouter } from './auth';
import * as accessTokenMiddleware from '../middlewares/access_token';

const ROUTER = express.Router();
ROUTER.use("/auth", getRouter());

ROUTER.use(accessTokenMiddleware.hook());
ROUTER.get("/test", (req, res, next) => {
    let params = doParseRequest<any, any>(req);
    doSend(res, new TestModel(params));
});

ROUTER.get("/error", (req, res, next) => {
    next(new Error("test_error"));
});

export function route() {
    return ROUTER;
}