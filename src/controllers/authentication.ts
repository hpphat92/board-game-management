import { injectable } from "tsyringe";
import { MemcacheProvider } from "../services/memcache";
import { LoggingProvider } from "../services/logger";
import { AuthRequest, AuthModel } from "../models/auth.model";
import { UserModel } from "../models/memcache";
import jwt from "jsonwebtoken";

@injectable()
export class AuthController {
    constructor(private _memcache: MemcacheProvider,
        private _logger: LoggingProvider) {

    }

    async doRegister(args: AuthRequest) {
        this._logger.log(args.email, "doRegister");
        this._memcache.save<UserModel>("accounts", args);

        let token = await this.getToken(args.email, "abc");
        return new AuthModel(token, args.email);
    }

    async doAuth(args: AuthRequest) {
        this._logger.log(args.email, "doAuth");
        let users = this._memcache.get<UserModel>("accounts", { email: args.email });

        let token = await this.getToken(args.email, "abc");
        return new AuthModel(token, users[0].email);
    }

    async getToken(email: string, signerKey: string) {
        return Buffer.from(jwt.sign({
            email,
            time: Date.now()
        }, signerKey, { expiresIn: 60 * 60 * 1000 })).toString("base64");
    }
}