export class MemcacheEvent {
    event: string;
    time: number;
}

export class UserModel {
    email: string;
    password: string;
}