import { BaseResponse, HttpRequestModel } from "../lib/server";

export class AuthModel extends BaseResponse {
    constructor(private _accessToken: string, private _user: string) {
        super();
    }

    toJson() {
        return {
            user: this._user,
            accessToken: this._accessToken
        }
    }
}

export class AuthRequest {
    email: string;
    password: string;
}