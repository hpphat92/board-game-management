import { Logger, WINSTON_LOG_LEVEL } from '../lib/log';
import { injectable } from 'tsyringe';
import { Configurations } from './config_service';

@injectable()
export class LoggingProvider {
    private _logger: Logger;

    constructor(private _config: Configurations) {
        this._logger = new Logger(this._config.getLogDir());
    }

    log(message: string, tag: string = 'generic') {
        this._logger.writeLog(tag, message, WINSTON_LOG_LEVEL.info);
    }

    error(message: string, tag: string = 'generic') {
        this._logger.writeLog(tag, message, WINSTON_LOG_LEVEL.error);
    }
}