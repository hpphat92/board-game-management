import { singleton } from "tsyringe";
import * as config from '../../config';

@singleton()
export class Configurations {
    getServerPort() {
        return config.APP_CONFIG.PORT;
    }

    getDebugging() {
        return config.APP_CONFIG.DEBUG;
    }

    getCacheStore() {
        return config.APP_CONFIG.MEMCACHE_STORE;
    }

    getLogDir() {
        return config.APP_CONFIG.LOG_DIR;
    }
}