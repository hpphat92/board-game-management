import { singleton, injectable } from "tsyringe";
import loki from 'lokijs';
import { Configurations } from "./config_service";
import { MemcacheEvent } from "../models/memcache";
import _ from "lodash";
import fs from 'fs';
import path from 'path';
import mkdirp from 'mkdirp';
import { LoggingProvider } from "./logger";

@singleton()
@injectable()
export class MemcacheProvider {
    private _database: loki;

    constructor(private _config: Configurations, private _logger: LoggingProvider) {
        let dir = this._checkStoreExist(this._config.getCacheStore());
        this._database = new loki(dir,
            {
                autosave: true,
                autoload: true,
                autosaveInterval: 10 * 1000,
                serializationMethod: "pretty",
                env: "NODEJS",
                autoloadCallback: this._initializeDatabase.bind(this),
                autosaveCallback: this._onAutoSave.bind(this)
            })
    }

    private _checkStoreExist(dir: string) {
        if (!fs.existsSync(dir)) {
            mkdirp(dir, (err) => {
                if (err) {
                    console.log(err);
                }
            });
        }

        return path.join(dir, 'store.json');
    }

    private _initializeDatabase() {
        this._logger.log("auto load");
        this.save<MemcacheEvent>("log", { event: 'dbinit', time: Date.now() });
    }

    private _onAutoSave(err?: any) {
        this._logger.log("auto save");
    }

    private _getCollection<T extends object>(name: string) {
        let collection = this._database.getCollection<T>(name);
        if (!collection) {
            collection = this._database.addCollection<T>(name);
        }

        return collection;
    }

    save<T extends object>(collectionName: string, val: T) {
        return this._getCollection<T>(collectionName)
            .insert(val);
    }

    get<T extends object>(collectionName: string, query: any) {
        return this._getCollection<T>(collectionName)
            .find(query);
    }
}